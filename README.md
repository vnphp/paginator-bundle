# Vnphp Paginator bundle


[![build status](https://gitlab.com/vnphp/paginator-bundle/badges/master/build.svg)](https://gitlab.com/vnphp/paginator-bundle/commits/master)
[![code quality](https://insight.sensiolabs.com/projects/8664e1be-0f9a-4a70-ab98-05c8420e2aba/big.png)](https://insight.sensiolabs.com/projects/8664e1be-0f9a-4a70-ab98-05c8420e2aba)


## Installation 

```
composer require vnphp/paginator-bundle
```

You need to have `git` installed. 

## Usage

### Add the bundle to your `AppKernel.php`:

```php
<?php 

$bundles = array(          
            new \Vnphp\PaginatorBundle\VnphpPaginatorBundle(),
        );

```

### Usage

```php
<?php

if ($page > 1) {
    $limit = 20;
    $paginatorOffset = -1;
} else {
    $limit = 19;
    $paginatorOffset = 0;
}

$this->paginator->paginate($query, $page, $limit, [
    'offset' => $paginatorOffset,
]);


