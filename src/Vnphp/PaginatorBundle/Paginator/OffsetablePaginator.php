<?php


namespace Vnphp\PaginatorBundle\Paginator;

use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OffsetablePaginator extends Paginator
{
    public function __construct(EventDispatcherInterface $eventDispatcher = null)
    {
        parent::__construct($eventDispatcher);

        $this->setDefaultPaginatorOptions(['offset' => 0]);
    }

    public function paginate($target, $page = 1, $limit = 10, array $options = array())
    {
        $limit = intval(abs($limit));
        if (!$limit) {
            throw new \LogicException("Invalid item per page number, must be a positive number");
        }

        $options = array_merge($this->defaultOptions, $options);

        $offset = abs($page - 1) * $limit;
        if ($options['offset']) {
            $offset += $options['offset'];
        }

        // default sort field and direction are set based on options (if available)
        if (!isset($_GET[$options['sortFieldParameterName']]) && isset($options['defaultSortFieldName'])) {
            $_GET[$options['sortFieldParameterName']] = $options['defaultSortFieldName'];

            if (!isset($_GET[$options['sortDirectionParameterName']])) {
                $_GET[$options['sortDirectionParameterName']] = isset($options['defaultSortDirection']) ? $options['defaultSortDirection'] : 'asc';
            }
        }

        // before pagination start
        $beforeEvent = new Event\BeforeEvent($this->eventDispatcher);
        $this->eventDispatcher->dispatch('knp_pager.before', $beforeEvent);
        // items
        $itemsEvent = new Event\ItemsEvent($offset, $limit);
        $itemsEvent->options = &$options;
        $itemsEvent->target = &$target;
        $this->eventDispatcher->dispatch('knp_pager.items', $itemsEvent);
        if (!$itemsEvent->isPropagationStopped()) {
            throw new \RuntimeException('One of listeners must count and slice given target');
        }
        // pagination initialization event
        $paginationEvent = new Event\PaginationEvent;
        $paginationEvent->target = &$target;
        $paginationEvent->options = &$options;
        $this->eventDispatcher->dispatch('knp_pager.pagination', $paginationEvent);
        if (!$paginationEvent->isPropagationStopped()) {
            throw new \RuntimeException('One of listeners must create pagination view');
        }
        // pagination class can be diferent, with diferent rendering methods
        $paginationView = $paginationEvent->getPagination();
        $paginationView->setCustomParameters($itemsEvent->getCustomPaginationParameters());
        $paginationView->setCurrentPageNumber($page);
        $paginationView->setItemNumberPerPage($limit);
        $paginationView->setTotalItemCount($itemsEvent->count);
        $paginationView->setPaginatorOptions($options);
        $paginationView->setItems($itemsEvent->items);

        // after
        $afterEvent = new Event\AfterEvent($paginationView);
        $this->eventDispatcher->dispatch('knp_pager.after', $afterEvent);
        return $paginationView;
    }
}
